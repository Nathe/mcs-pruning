/*
 * MO_PSO_Sampler.h
 *
 *  Created on: Mar 4, 2011
 *      Author: rgreen
 */

#ifndef MO_PSO_SAMPLER_H_
#define MO_PSO_SAMPLER_H_

#include "PSO_Sampler.h"
#include "MO_BinaryParticle.h"

class MO_PSO_Sampler: public PSO_Sampler {
	public:
		MO_PSO_Sampler(int _Np, int _Nd, int _Nt);
		virtual ~MO_PSO_Sampler();
		virtual void run(MTRand& mt);
		bool checkConvergence();

	protected:
		void 	initPopulation(MTRand& mt);
		void 	evaluateFitness();
		void 	updatePositions(MTRand& mt);


		std::vector <MO_BinaryParticle > swarm;
		std::vector < std::vector < int > > gBest;
		std::vector < double > gBestValues;

		double cFactor, C3, C4;
};


#endif /* MO_PSO_SAMPLER_H_ */
